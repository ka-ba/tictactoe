/*
 * Board.cpp
 *
 *  Created on: 7 Aug 2021
 *      Author: kaba
 */

#include <cassert>
#include "Board.h"

Board::Board() : randomEngine(std::default_random_engine{std::random_device{}()}) {
    reset();
}

void Board::reset() {
    forAllSpots( [](auto &f){f=Side::EMPTY;} );
}

std::string Board::rowString( size_t row_num ) const {
    switch( row_num) {
    case 0:
    case 4:
        return "+---+";
    case 1:
    case 2:
    case 3: {
        auto const &row = fields[row_num-1];
        std::stringstream eam;
        eam << "|";
        for( auto const &f : row )
            eam << fieldChar[static_cast<size_t>(f)];
        eam << "|";
        return eam.str();
    }
    default:
        throw std::invalid_argument{ std::string{"row number invalid: "}.append(std::to_string(row_num)) };
    }
}

std::vector<BoardCoord> Board::emptySpots() const {
    std::vector<BoardCoord> e_spots{BOARDEDGESIZE*BOARDEDGESIZE};
    e_spots.clear();
    forAllCoords( [&e_spots,this](auto const &c){
        if( Side::EMPTY == (*this)[c] )
            e_spots.push_back( c );
    } );
    return e_spots;
}

std::optional<BoardCoord> Board::randomEmptySpot() const {
    auto spots = emptySpots();
    if( spots.empty() )
        return std::nullopt;
    std::uniform_int_distribution<size_t> dist{0,spots.size()-1};
    return spots.at( dist(randomEngine) );
}

void Board::move( BoardCoord const &coord, Side side ) {
    if( ! coord.valid() )
        throw std::invalid_argument{ "cannot move onto invalid coord" };
    if( Side::EMPTY != (*this)[coord] )
        throw std::invalid_argument{ "cannot move onto already occupied coord" };
    (*this)[coord] = side;
}

bool Board::valid() const {
    std::array<size_t,1u+static_cast<uint>(Side::NOUGHT)> counts;
    std::fill( counts.begin(), counts.end(), 0 );
    forAllSpots( [&counts](auto const &f){++counts[static_cast<uint>(f)];} );
    return counts[static_cast<uint>(Side::NOUGHT)] == (BOARDEDGESIZE*BOARDEDGESIZE-counts[static_cast<uint>(Side::EMPTY)])/2;
}

GameResult Board::result() const {
    size_t cross_threes{}, cross_diag1{}, cross_diag2{}, // @suppress("Multiple variable declaration")
    nought_threes{}, nought_diag1{}, nought_diag2{}, empties{}; // @suppress("Multiple variable declaration")
    for( size_t i=0; i<BOARDEDGESIZE; ++i ) {
        size_t cross_rows{}, cross_cols{}, nought_rows{}, nought_cols{}; // @suppress("Multiple variable declaration")
        for( size_t j=0; j<BOARDEDGESIZE; ++j ) {
            if( Side::CROSS == (*this)[BoardCoord{j,i}]) ++cross_rows;
            if( Side::CROSS == (*this)[BoardCoord{i,j}]) ++cross_cols;
            if( Side::NOUGHT == (*this)[BoardCoord{j,i}]) ++nought_rows;
            if( Side::NOUGHT == (*this)[BoardCoord{i,j}]) ++nought_cols;
            if( Side::EMPTY == (*this)[BoardCoord{i,j}]) ++empties;
        }
        cross_threes += (cross_rows/BOARDEDGESIZE) + (cross_cols/BOARDEDGESIZE);
        nought_threes += (nought_rows/BOARDEDGESIZE) + (nought_cols/BOARDEDGESIZE);
        if( Side::CROSS == (*this)[BoardCoord{i,i}]) ++cross_diag1;
        if( Side::CROSS == (*this)[BoardCoord{i,BOARDEDGESIZE-1-i}]) ++cross_diag2;
        if( Side::NOUGHT == (*this)[BoardCoord{i,i}]) ++nought_diag1;
        if( Side::NOUGHT == (*this)[BoardCoord{i,BOARDEDGESIZE-1-i}]) ++nought_diag2;
    }
    cross_threes += (cross_diag1/BOARDEDGESIZE) + (cross_diag2/BOARDEDGESIZE);
    nought_threes += (nought_diag1/BOARDEDGESIZE) + (nought_diag2/BOARDEDGESIZE);
    assert( ! (cross_threes&&nought_threes) );
    if( cross_threes )
        return GameResult::CROSS_WINS;
    if( nought_threes )
        return GameResult::NOUGHT_WINS;
    if( empties )
        return GameResult::ONGOING;
    return GameResult::DRAW;
}

const Side& Board::operator []( const BoardCoord &coord ) const {
    if( ! coord.valid() )
        throw std::invalid_argument{ "do not access a board with invalid coords" };
    return fields[coord.r][coord.c];
}

Side& Board::operator []( const BoardCoord &coord ) {
    if( ! coord.valid() )
        throw std::invalid_argument{ "do not access a board with invalid coords" };
    return fields[coord.r][coord.c];
}

uint32_t Board::as18Bits() const {
    uint32_t representation{1};
    forAllSpots( [&representation](auto const &s){representation<<=2;representation|=static_cast<uint8_t>(s)&0b11u;} );
    return representation;
}

Side Board::otherSide( Side side ) {
    if( Side::CROSS == side ) return Side::NOUGHT;
    if( Side::NOUGHT == side ) return Side::CROSS;
    throw std::invalid_argument{ "EMPTY has no other side" };
}

Side Board::winner( GameResult result ) {
    switch( result ) {
    case GameResult::CROSS_WINS:
        return Side::CROSS;
    case GameResult::NOUGHT_WINS:
        return Side::NOUGHT;
    default:
        return Side::EMPTY; // synonym for no-one
    }
}

std::ostream &operator<<( std::ostream &out, Board const &board ) {
    out << "\n";
    for( size_t i=0; i<BOARDEDGESIZE+2; ++i )
        out << "  " << board.rowString( i ) << "\n";
    return out;
}

std::ostream& operator<<( std::ostream &out, GameResult const &result ) {
    // ONGOING, DRAW, CROSS_WINS, NOUGHT_WINS
    switch( result ) {
    case GameResult::ONGOING:
        out << "ongoing";
        break;
    case GameResult::DRAW:
        out << "draw";
        break;
    case GameResult::CROSS_WINS:
        out << "cross wins";
        break;
    case GameResult::NOUGHT_WINS:
        out << "nought wins";
        break;
    }
    return out;
}
