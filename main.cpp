/*
 * main.cpp
 *
 *  Created on: 7 Aug 2021
 *      Author: kaba
 */

#include <cassert>
#include <iostream>
#include <string>
#include "Statistics.h"
#include "MinMaxPlayer.h"
#include "RandomPlayer.h"

int main( int argc, char **argv ) {
    // FIXME: later maybe use boost::program_options ...
    if( 2 != argc ) {
        std::cout << "missing invocation count\nusage: " << argv[0] << " [invocation count]\n";
        return 1;
    }
    auto invocations = std::stoul( argv[1] );
    Statistics stat;
//    auto results = stat.runMany( invocations, std::make_unique<RandomPlayer>(), std::make_unique<RandomPlayer>() );
//    auto results = stat.runMany( invocations, std::make_unique<MinMaxPlayer>(), std::make_unique<MinMaxPlayer>() );
    auto results = stat.runMany( invocations, std::make_unique<RandomPlayer>(), std::make_unique<MinMaxPlayer>() );
    std::cout << results << "\n";
}


