/*
 * RandomPlayer.cpp
 *
 *  Created on: 8 Aug 2021
 *      Author: kaba
 */

#include "RandomPlayer.h"

RandomPlayer::~RandomPlayer() {
}

std::unique_ptr<Player> RandomPlayer::clone() const {
    return std::make_unique<RandomPlayer>( *this );
}

void RandomPlayer::doMoveImpl( Board &board ) const {
    board.move( board.randomEmptySpot().value(), side_ );
}
