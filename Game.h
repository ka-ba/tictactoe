/*
 * Game.h
 *
 *  Created on: 8 Aug 2021
 *      Author: kaba
 */

#pragma once
#include <memory>
#include <vector>
#include "Board.h"
#include "Player.h"

struct GameOutcome
{
    GameResult result;
};

class Game
{
public:
    Game( std::unique_ptr<Player> p1, std::unique_ptr<Player> p2 );
    GameOutcome play();
private:
    friend std::ostream &operator<<( std::ostream &out, Game const &game );
    std::vector<Board> history{1};
    std::unique_ptr<Player> player1, player2; // @suppress("Multiple variable declaration")
};

