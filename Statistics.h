/*
 * Statistics.h
 *
 *  Created on: 8 Aug 2021
 *      Author: kaba
 */

#pragma once

#include <memory>
#include "Player.h"

struct StatisticsResult
{
    size_t runs{}, draws{}, crossWins{}, noughtWins{}, errors{}; // @suppress("Multiple variable declaration")
    StatisticsResult() {}
    StatisticsResult( StatisticsResult const &other ) : runs(other.runs), draws(other.draws), crossWins(other.crossWins), noughtWins(other.noughtWins), errors(other.errors) {}
    StatisticsResult( size_t _runs, size_t _draws, size_t cross_wins, size_t nought_wins, size_t _errors )
    : runs(_runs), draws(_draws), crossWins(cross_wins), noughtWins(nought_wins), errors(_errors) {}
    StatisticsResult combine( StatisticsResult const &other ) const;
};

class Statistics
{
public:
    static StatisticsResult runMany( size_t const runs, std::unique_ptr<Player> const player1, std::unique_ptr<Player> const player2 );
};

std::ostream &operator<<( std::ostream &out, StatisticsResult const &results );
