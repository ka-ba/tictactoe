/*
 * Player.cpp
 *
 *  Created on: 8 Aug 2021
 *      Author: kaba
 */

#include <cassert>
#include "Player.h"

Player::~Player() {
}

void Player::assign( Side const side ) {
    if( Side::EMPTY != side_ )
        throw std::logic_error{ "not allowed to re-assign a side" };
    side_ = side;
}

void Player::doMove( Board &board ) {
    if( Side::EMPTY == side_ )
        throw std::logic_error{ "must assign a side before moving" };
    assert( GameResult::ONGOING == board.result() );
    doMoveImpl( board );
}
