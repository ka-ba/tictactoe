/*
 * Board.h
 *
 *  Created on: 7 Aug 2021
 *      Author: kaba
 */

#pragma once
#include <array>
#include <optional>
#include <random>
#include <string>
#include <sstream>
#include <vector>

constexpr size_t BOARDEDGESIZE = 3;
enum class GameResult : uint8_t { ONGOING, DRAW, CROSS_WINS, NOUGHT_WINS };
enum class Side : uint8_t { EMPTY, CROSS, NOUGHT };

struct BoardCoord
{
    size_t r, c; // @suppress("Multiple variable declaration")
    bool valid() const { return (r<BOARDEDGESIZE) && (c<BOARDEDGESIZE); }
};

class Board
{
public:
    Board();
    void reset();
    Side const &operator[]( BoardCoord const & coord ) const;
    std::vector<BoardCoord> emptySpots() const;
    std::optional<BoardCoord> randomEmptySpot() const;
    void move( BoardCoord const &coord, Side side );
    GameResult result() const;
    Side winner() const { return winner(result()); }
    bool valid() const;
    uint32_t as18Bits() const;
    std::string rowString( size_t row_num) const;
    static Side otherSide( Side side );
    static Side winner( GameResult result );
private:
    template<typename OPER>
    void forAllSpots( OPER operation );
    template<typename OPER>
    void forAllSpots( OPER operation ) const;
    template<typename OPER>
    void forAllCoords( OPER operation );
    template<typename OPER>
    void forAllCoords( OPER operation ) const;
    Side &operator[]( BoardCoord const & coord );
    friend std::ostream &operator<<( std::ostream &out, Board const &board );
    std::array<std::array<Side,BOARDEDGESIZE>,BOARDEDGESIZE> fields;
    std::array<char,3> fieldChar = { ' ', 'x', 'o' }; // @suppress("Avoid magic numbers")
    mutable std::default_random_engine randomEngine;
};

template< typename OPER >
inline void Board::forAllSpots( OPER operation ) {
    for( auto &row : fields )
        for( auto &f : row )
            operation( f );
}

template< typename OPER >
inline void Board::forAllSpots( OPER operation ) const {
    for( auto &row : fields )
        for( auto &f : row )
            operation( f );
}

template< typename OPER >
inline void Board::forAllCoords( OPER operation ) {
    for( size_t row=0; row<BOARDEDGESIZE; ++row )
        for( size_t col=0; col<BOARDEDGESIZE; ++col )
            operation( BoardCoord{row,col} );
}

template< typename OPER >
inline void Board::forAllCoords( OPER operation ) const {
    for( size_t row=0; row<BOARDEDGESIZE; ++row )
        for( size_t col=0; col<BOARDEDGESIZE; ++col )
            operation( BoardCoord{row,col} );
}

std::ostream &operator<<( std::ostream &out, GameResult const &result );
