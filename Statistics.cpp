/*
 * Statistics.cpp
 *
 *  Created on: 8 Aug 2021
 *      Author: kaba
 */

#include "Statistics.h"
#include <cassert>
#include <execution>
#include <numeric>
#include "Game.h"

StatisticsResult Statistics::runMany( size_t const runs, std::unique_ptr<Player> const player1, std::unique_ptr<Player> const player2 ) {
    // unfortunately, without c++20 we seem to need a collection to use concurrency ...
    std::vector<bool> waste( runs );
    assert( runs == waste.size() );
    auto result = std::transform_reduce( std::execution::par, waste.begin(), waste.end(), StatisticsResult{},
                                          [](StatisticsResult const &lhs,StatisticsResult const &rhs){
        return lhs.combine( rhs );
    },
                                          [&player1,&player2]([[maybe_unused]]auto const &w){
        try {
            Game game{ player1->clone(), player2->clone() };
            auto outcome = game.play();
            switch( outcome.result ) {
            case GameResult::DRAW:
                return StatisticsResult{0,1,0,0,0};
            case GameResult::CROSS_WINS:
                return StatisticsResult{0,0,1,0,0};
            case GameResult::NOUGHT_WINS:
                return StatisticsResult{0,0,0,1,0};
            default:
                return StatisticsResult{0,0,0,0,1};
            }
        } catch( std::exception const &e ) {
            return StatisticsResult{0,0,0,0,1};
        }
    } );
    result.runs = runs;
    return result;
}

StatisticsResult StatisticsResult::combine( StatisticsResult const &other ) const {
    StatisticsResult combination{ *this };
    combination.draws += other.draws;
    combination.crossWins += other.crossWins;
    combination.noughtWins += other.noughtWins;
    combination.errors += other.errors;
    return combination;
}

std::ostream& operator <<( std::ostream &out, StatisticsResult const &results ) {
    out << "matches: " << results.runs;
    out << ", draws: " << results.draws;
    out << ", cross wins: " << results.crossWins;
    out << ", nought wins: " << results.noughtWins;
    if( results.errors )
        out << ", errors: " << results.errors;
    return out;
}
