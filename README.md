# Tic Tac Toe

Re-implementation of the TicTacToe ML example of Carsten Friedrich in c++; maybe only up to part 3, because installing TensorFlow for use from something else than python seems a hassle.

# Credits:

[Blog series](https://medium.com/@carsten.friedrich/part-1-computer-tic-tac-toe-basics-35964f92fa03) of Carsten Friedrich