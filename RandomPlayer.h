/*
 * RandomPlayer.h
 *
 *  Created on: 8 Aug 2021
 *      Author: kaba
 */

#pragma once
#include "Player.h"

class RandomPlayer : public Player
{
public:
    RandomPlayer() : Player() {}
    RandomPlayer( RandomPlayer const &other ) : Player(other) {}
    virtual ~RandomPlayer();
    std::unique_ptr<Player> clone() const override;
protected:
    void doMoveImpl( Board &board ) const override;
};
