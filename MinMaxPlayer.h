/*
 * MinMaxPlayer.h
 *
 *  Created on: 10 Aug 2021
 *      Author: kaba
 */

#pragma once

#include "Player.h"
#include <map>
#include <mutex>

class MinMaxPlayer: public Player
{
public:
    MinMaxPlayer() : Player(), bestMove(moveMapSingleton()) {}
    MinMaxPlayer( MinMaxPlayer const &other ) : Player(other), bestMove(moveMapSingleton()) {}
    MinMaxPlayer &operator=( MinMaxPlayer const& ) = delete;
    virtual ~MinMaxPlayer();
    std::unique_ptr<Player> clone() const override;
protected:
    void doMoveImpl( Board &board ) const override;
private:
    static std::map<int32_t,BoardCoord> &moveMapSingleton();
    std::map<int32_t,BoardCoord> &bestMove;
};

