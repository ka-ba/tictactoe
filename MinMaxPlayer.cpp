/*
 * MinMaxPlayer.cpp
 *
 *  Created on: 10 Aug 2021
 *      Author: kaba
 */

#include "MinMaxPlayer.h"
#include <cassert>

namespace MinMaxPlayer_private {

int8_t recordBestMove( Board const &starting, Side side, std::map<int32_t,BoardCoord> &move_map ) {
    auto const result = starting.result();
    if( GameResult::ONGOING == result ) {
        std::vector<std::vector<BoardCoord>> scores{3};
        assert( 3 == scores.size() );
        for( auto const &s : starting.emptySpots() ) {
            auto moved = starting;
            moved.move( s, side );
            auto const score = recordBestMove(moved,Board::otherSide(side),move_map) * -1;
            assert( (-2<score) && (score<2) );
            scores[ 1-score ].push_back( s ); // best first
        }
        assert( 0 < (scores[0].size()+scores[1].size()+scores[2].size()) );
        for( size_t i=0; i< scores.size(); ++i )
            if( ! scores[i].empty() ) {
                move_map[starting.as18Bits()] = scores[i].front(); // TODO: make more flexible l8ter
                return 1-i;
            }
        throw std::logic_error{ "may never touch this line!" };
    } else
        switch( result ) {
        case GameResult::DRAW:
            return 0;
        case GameResult::CROSS_WINS: // [[fallthrough]]
        case GameResult::NOUGHT_WINS:
            return side==Board::winner(result) ? 1 : -1;
        default:
            throw std::logic_error{ "must be finished and can't reach this point when finished" };
        }
}

}  // namespace MinMaxPlayer_private

MinMaxPlayer::~MinMaxPlayer() {
}

using namespace MinMaxPlayer_private;

std::unique_ptr<Player> MinMaxPlayer::clone() const {
    return std::make_unique<MinMaxPlayer>( *this );
}

void MinMaxPlayer::doMoveImpl( Board &board ) const {
    board.move( bestMove.at(board.as18Bits()), side_ );
}

std::map<int32_t,BoardCoord>& MinMaxPlayer::moveMapSingleton() {
    static std::map<int32_t,BoardCoord> best_move_singleton;
    static std::mutex map_mutex;
    std::lock_guard<std::mutex> guard{map_mutex};
    if( best_move_singleton.empty() )
        recordBestMove( Board{}, Side::CROSS, best_move_singleton );
    return best_move_singleton;
}
