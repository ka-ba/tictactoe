/*
 * Game.cpp
 *
 *  Created on: 8 Aug 2021
 *      Author: kaba
 */

#include <cassert>
#include "Game.h"

Game::Game( std::unique_ptr<Player> p1, std::unique_ptr<Player> p2 ) : player1(std::move(p1)), player2(std::move(p2)) {
    if( player1->isAssigned() || player2->isAssigned() )
        throw std::invalid_argument{ "will need unassigned players to start a game" };
    player1->assign( Side::CROSS );
    player2->assign( Side::NOUGHT );
}

GameOutcome Game::play() {
    history.clear();
    history.emplace_back();
    Player *on_turn;
    do {
        on_turn = on_turn==player1.get() ? player2.get() : player1.get();
        history.push_back( history.back() );
        on_turn->doMove( history.back() );
        assert( history.back().valid() );
    } while( GameResult::ONGOING == history.back().result() );
    return { history.back().result() };
}

std::ostream &operator<<( std::ostream &out, Game const &game ) {
    out << "\n";
    for( size_t i=0; i<BOARDEDGESIZE+2; ++i ) {
        for( auto const &board : game.history )
            out << "  " << board.rowString( i );
        out << "\n";
    }
    return out;
}
