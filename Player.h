/*
 * Player.h
 *
 *  Created on: 8 Aug 2021
 *      Author: kaba
 */

#pragma once
#include <memory>
#include "Board.h"

class Player
{
public:
    Player() : side_(Side::EMPTY) {}
    Player( Player const &other ) : side_(other.side_) {}
    virtual ~Player();
    virtual std::unique_ptr<Player> clone() const =0;
    void assign( Side const side );
    void doMove( Board &board );
    bool isAssigned() { return Side::EMPTY != side_; }
protected:
    virtual void doMoveImpl( Board &board ) const =0;
    Side side_;
};
